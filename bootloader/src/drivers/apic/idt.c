#include "idt.h"

void create_idt()
{
    blank_idt();
    create_fault_handlers();
}
void blank_idt()
{
    IDTDescriptor *descriptors = (IDTDescriptor*)0x20000;
    for(int i=0; i< 256;++i)
    {
        descriptors[i].offset_1 = 0;
        descriptors[i].selector = GDT_CODE_SELECTOR;
        descriptors[i].type_attr = IDT_INTERRUPT_TYPE;
        descriptors[i].offset_2 = 0;
    }
}
void create_fault_handlers()
{
    //install_interrupt_service((uint32_t*)0x40000,2);
    //remove_interrupt_service(2);

    uint16_t *ptr2 = (uint16_t*)0x15000;
    *ptr2 = (uint16_t)CLI_HLT_OPCODES; // assembly cli hlt
    
    install_isr((uint32_t*)0x40000,(uint16_t*)0x15000);

    install_interrupt_service((uint32_t*)0x40000,DOUBLE_FAULT_VECTOR);
    //install_interrupt_service((uint32_t*)0x40000,NMI_VECTOR);
    //remove_interrupt_service(NMI_VECTOR);
    //remove_interrupt_service(DOUBLE_FAULT_VECTOR);

}

void install_isr(uint32_t *isr_addr,uint32_t *func_addr_ptr)
{
    ISRHandler *ptr = (ISRHandler*)isr_addr;
    ptr->call =  (uint16_t)CALL_MEMORY_OPCODE; // assembly call [memory] opcode
    ptr->call_addr = (uint32_t)(&ptr->func_addr);
    ptr->iret = ((uint8_t)IRETD_OPCODE); // assembly iretd
    ptr->func_addr = func_addr_ptr;
}

void install_interrupt_service(uint32_t *pointer,uint16_t index)
{
    IDTDescriptor *descriptors = (IDTDescriptor*)0x20000;
    uint16_t lowptr = (uint16_t)((uint32_t)pointer & 0xffff);
    uint16_t highptr = (uint16_t)((uint32_t)pointer >> 16);
    if(index <= 256)
    {
        descriptors[index].offset_1 = lowptr;
        descriptors[index].selector = GDT_CODE_SELECTOR;
        descriptors[index].type_attr = IDT_INTERRUPT_PRESENT | IDT_INTERRUPT_TYPE;
        descriptors[index].offset_2 = highptr;
    }
}
void remove_interrupt_service(uint16_t index)
{
    IDTDescriptor *descriptors = (IDTDescriptor*)0x20000;
    if(index <= 256)
    {
        descriptors[index].type_attr = descriptors[index].type_attr & IDT_INTERRUPT_NOT_PRESENT;
    }
}